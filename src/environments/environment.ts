// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDjmSCwKjMsq9umYvW6lSHyMKLMlTMOB3Y",
    authDomain: "field-analyzer.firebaseapp.com",
    databaseURL: "https://field-analyzer.firebaseio.com",
    projectId: "field-analyzer",
    storageBucket: "field-analyzer.appspot.com",
    messagingSenderId: "183006410896",
    appId: "1:183006410896:web:3e892d20eeeaccd1274015",
    measurementId: "G-9JQCPM2MPX" 
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
