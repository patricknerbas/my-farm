import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FarmPageComponent } from './farm-page/farm-page.component';

const routes: Routes = [
  { path: '', component: FarmPageComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FarmRoutingModule { }
