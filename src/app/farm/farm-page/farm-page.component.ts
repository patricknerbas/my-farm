import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Farm } from 'src/app/shared/interfaces/farm';
import { Ground } from 'src/app/shared/interfaces/ground';
import { Harvest } from 'src/app/shared/interfaces/harvest';
import { FarmsService } from 'src/app/shared/services/farms.service';
import { GroundsService } from 'src/app/shared/services/grounds.service';
import { HarvestsService } from 'src/app/shared/services/harvests.service';

@Component({
  selector: 'app-farm-page',
  templateUrl: './farm-page.component.html',
  styleUrls: ['./farm-page.component.scss']
})
export class FarmPageComponent implements OnInit  {
  selectedFarm: Farm;
  selectedGround: Ground;

  farms$: Observable<Farm[]>; 
  grounds$: Observable<Ground[]>; 
  harvests$: Observable<Ground[]>; 

  constructor(
    private farmsService: FarmsService,
    private groundsService: GroundsService,
    private harvestsService: HarvestsService,
  ) {}

  ngOnInit(): void {
    this.loadFarms();
  }
  
  loadFarms(): void {
    this.farms$ = this.farmsService.getFarms();
  }
  
  loadGroundsByFarmId(farmId: string): void {
    this.grounds$ = this.groundsService.getGroundsByFarmId(farmId);
  }

  loadHarvestsByGroundId(groundId: string) {
    this.harvests$ = this.harvestsService.getHarvestsByGroundId(groundId);    
  }

  onEditFarm(farm: Farm): void {
    this.selectedFarm = farm;
    this.selectedGround = null;
    this.loadGroundsByFarmId(farm.id);
  }

  onRemoveFarm(farm: Farm): void {
    this.farmsService.removeFarm(farm.id);

    if(this.selectedFarm && this.selectedFarm.id == farm.id) {
      this.selectedFarm = null;
      this.selectedGround = null;
    }
  }

  onCreateFarm(farm: Farm): void {
    this.farmsService.createFarm(farm);
  }  

  onCreateGround(ground: Ground): void {
    this.groundsService.createGround(this.selectedFarm.id, ground)
  }    

  onEditGround(ground: Ground): void {
    this.selectedGround = ground;
    this.loadHarvestsByGroundId(ground.id);
  }

  onRemoveGround(ground: Ground): void {
    this.groundsService.onRemoveGround(ground);

    if(this.selectedGround && this.selectedGround.id == ground.id) {
      this.selectedGround = null;
    }    
  }  

  onCreateHarvest(harvest: Harvest): void {
    this.harvestsService.onCreateHarvest(this.selectedGround.id, harvest);
  }

  onRemoveHarvest(harvest: Harvest): void {
    this.harvestsService.onRemoveHarvest(harvest);
  }  

}
