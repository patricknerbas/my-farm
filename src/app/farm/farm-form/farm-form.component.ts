import { Component, EventEmitter, Output } from '@angular/core';
import { Farm } from 'src/app/shared/interfaces/farm';

@Component({
  selector: 'app-farm-form',
  templateUrl: './farm-form.component.html',
  styleUrls: ['./farm-form.component.scss']
})
export class FarmFormComponent {
  @Output() onCreateFarm: EventEmitter<Farm> = new EventEmitter();

  farm: Farm = {
    grounds: [],
    totalAmount: 0
  };

  createFarm(farm: Farm): void {
    this.onCreateFarm.emit(farm);
  }

  clearForm() {
    this.farm = {
      grounds: [],
      totalAmount: 0
    };
  }

  submitForm(farm: Farm) {
    this.clearForm();
    this.createFarm(farm);
  }
}
