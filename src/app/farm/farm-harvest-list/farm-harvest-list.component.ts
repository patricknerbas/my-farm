import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Harvest } from 'src/app/shared/interfaces/harvest';

@Component({
  selector: 'app-farm-harvest-list',
  templateUrl: './farm-harvest-list.component.html',
  styleUrls: ['./farm-harvest-list.component.scss']
})
export class FarmHarvestListComponent {
  @Input() harvests: Harvest[] = [];
  @Output() onRemoveHarvest: EventEmitter<Harvest> = new EventEmitter();  

  removeHarvest(harvest: Harvest): void {
    this.onRemoveHarvest.emit(harvest);
  }  

}
