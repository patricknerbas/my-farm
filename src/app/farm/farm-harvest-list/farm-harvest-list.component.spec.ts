import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmHarvestListComponent } from './farm-harvest-list.component';

describe('FarmHarvestListComponent', () => {
  let component: FarmHarvestListComponent;
  let fixture: ComponentFixture<FarmHarvestListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FarmHarvestListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmHarvestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
