import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FarmRoutingModule } from './farm-routing.module';
import { FarmPageComponent } from './farm-page/farm-page.component';
import { SharedModule } from '../shared/shared.module';
import { FarmFormComponent } from './farm-form/farm-form.component';
import { FarmGroundFormComponent } from './farm-ground-form/farm-ground-form.component';
import { FarmHarvestFormComponent } from './farm-harvest-form/farm-harvest-form.component';
import { FarmListComponent } from './farm-list/farm-list.component';
import { FarmGroundListComponent } from './farm-ground-list/farm-ground-list.component';
import { FarmHarvestListComponent } from './farm-harvest-list/farm-harvest-list.component';

@NgModule({
  declarations: [
    FarmPageComponent,
    FarmFormComponent,
    FarmGroundFormComponent,
    FarmHarvestFormComponent,
    FarmListComponent,
    FarmGroundListComponent,
    FarmHarvestListComponent
  ],
  imports: [
    CommonModule,
    FarmRoutingModule,
    SharedModule,
  ]
})
export class FarmModule { }
