import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Harvest } from 'src/app/shared/interfaces/harvest';

@Component({
  selector: 'app-farm-harvest-form',
  templateUrl: './farm-harvest-form.component.html',
  styleUrls: ['./farm-harvest-form.component.scss']
})
export class FarmHarvestFormComponent {
  harvest: Harvest = {};
  @Output() onCreateHarvest: EventEmitter<Harvest> = new EventEmitter();

  createHarvest(harvest: Harvest): void {
    this.onCreateHarvest.emit(harvest)
  }

  clearForm() {
    this.harvest = {};
  }

  formatLabelSlider(amount: number): string {
    return `$${amount}`;
  }  

  submitForm(harvest: Harvest) {
    this.clearForm();
    this.createHarvest(harvest);
  }
}
