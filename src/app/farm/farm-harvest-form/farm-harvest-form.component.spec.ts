import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmHarvestFormComponent } from './farm-harvest-form.component';

describe('FarmHarvestFormComponent', () => {
  let component: FarmHarvestFormComponent;
  let fixture: ComponentFixture<FarmHarvestFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FarmHarvestFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmHarvestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
