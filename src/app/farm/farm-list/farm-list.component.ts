import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Farm } from 'src/app/shared/interfaces/farm';

@Component({
  selector: 'app-farm-list',
  templateUrl: './farm-list.component.html',
  styleUrls: ['./farm-list.component.scss']
})
export class FarmListComponent {
  @Input() farms: Farm[] = [];
  @Input() selectedFarm: Farm = {};
  @Output() onEditFarm: EventEmitter<Farm> = new EventEmitter();
  @Output() onRemoveFarm: EventEmitter<Farm> = new EventEmitter();  

  editFarm(farm: Farm) {
    this.onEditFarm.emit(farm);
  }

  removeFarm(farm: Farm) {
    this.onRemoveFarm.emit(farm);
  }    
}
