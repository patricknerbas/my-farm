import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmGroundFormComponent } from './farm-ground-form.component';

describe('FarmGroundFormComponent', () => {
  let component: FarmGroundFormComponent;
  let fixture: ComponentFixture<FarmGroundFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FarmGroundFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmGroundFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
