import { Component, EventEmitter, Output } from '@angular/core';
import { Ground } from 'src/app/shared/interfaces/ground';

@Component({
  selector: 'app-farm-ground-form',
  templateUrl: './farm-ground-form.component.html',
  styleUrls: ['./farm-ground-form.component.scss']
})
export class FarmGroundFormComponent {
  @Output() onCreateGround: EventEmitter<Ground> = new EventEmitter();

  ground: Ground = {
    harvests: [],
    totalAmount: 0
  };
  
  submitForm(ground: Ground) {
    this.clearForm();
    this.addGround(ground);
  }
  
  clearForm() {
    this.ground = {
      harvests: [],
      totalAmount: 0
    };
  }

  formatLabelSlider(amount: number): string {
    return `${amount}m`;
  }

  addGround(ground: Ground): void {
    this.onCreateGround.emit(ground);
  }
}
