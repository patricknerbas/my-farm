import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Ground } from 'src/app/shared/interfaces/ground';

@Component({
  selector: 'app-farm-ground-list',
  templateUrl: './farm-ground-list.component.html',
  styleUrls: ['./farm-ground-list.component.scss']
})
export class FarmGroundListComponent {
  @Input() grounds: Ground[] = [];
  @Input() selectedGround: Ground = {};
  @Output() onEditGround: EventEmitter<Ground> = new EventEmitter();
  @Output() onRemoveGround: EventEmitter<Ground> = new EventEmitter();

  editGround(ground: Ground): void {
    this.onEditGround.emit(ground);
  }  

  removeGround(ground: Ground): void {
    this.onRemoveGround.emit(ground);
  }      
}
