import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmGroundListComponent } from './farm-ground-list.component';

describe('FarmGroundListComponent', () => {
  let component: FarmGroundListComponent;
  let fixture: ComponentFixture<FarmGroundListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FarmGroundListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmGroundListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
