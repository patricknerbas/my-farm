import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularFireModule } from '@angular/fire'

import { SharedRoutingModule } from './shared-routing.module';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { ShellComponent } from './shell/shell.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';

const components = [ShellComponent];
const modules = [
  CommonModule,
  SharedRoutingModule,
  AngularFireModule,
  FormsModule,
  MatCardModule,
  MatFormFieldModule,
  MatSliderModule,
  MatButtonModule,
  MatInputModule,
  MatListModule,  
  MatDividerModule, 
  MatIconModule,
  LayoutModule,
  MatToolbarModule,
  MatSidenavModule,
]

@NgModule({
  declarations: [...components],
  imports: [...modules],
  exports: [...modules, ...components],
})
export class SharedModule { }
