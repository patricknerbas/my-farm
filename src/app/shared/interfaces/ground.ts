import { Harvest } from './harvest';

export interface Ground {
  id?: string;
  width?: number;
  length?: number;
  totalAmount?: number;
  harvests?: Harvest[];
  farmId?: string;
}
