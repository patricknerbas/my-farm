export interface Harvest {
  id?: string;
  name?: string;
  amount?: number;
  groundId?: string;
}
