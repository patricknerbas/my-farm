import { Ground } from './ground';

export interface Farm {
  id?: string;
  name?: string;
  totalAmount?: number;
  grounds?: Ground[];
}
