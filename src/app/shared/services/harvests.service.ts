import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { Harvest } from '../interfaces/harvest';
import { GroundsService } from '../services/grounds.service';
import { FarmsService } from '../services/farms.service';

@Injectable({
  providedIn: 'root'
})
export class HarvestsService {

  static collectionId = 'harvests';

  constructor(private db: AngularFirestore) {}

  createHarvest(groundId: string, harvest: Harvest): Promise<DocumentReference> {
    return this.db.collection(`${HarvestsService.collectionId}`)
      .add({ ...harvest, groundId })
  }

  updateHarvest(harvestId: string, harvest: Harvest): Promise<void> {
    return this.db.collection(`${HarvestsService.collectionId}`)
      .doc(harvestId)
      .update(harvest);
  }  

  removeHarvest(harvestId: string): Promise<void> {
    return this.db.collection(HarvestsService.collectionId)
      .doc(harvestId)
      .delete();
  }    

  getHarvests(): Observable<{ id: string; }[]> {
    return this.db.collection(HarvestsService.collectionId)
      .valueChanges({ idField: 'id' });
  }

  getHarvestById(harvestId: string): AngularFirestoreDocument<Harvest> {
    return this.db.collection(HarvestsService.collectionId)
      .doc(harvestId);    
  }

  getHarvestsByGroundId(groundId: string): Observable<Harvest[]> {
    return this.db.collection(HarvestsService.collectionId, ref =>
      ref.where('groundId', '==', groundId)
    )
    .valueChanges({ idField: 'id' })
  } 
  
  onCreateHarvest(groundId: string, harvest: Harvest): void {
    const groundsColletion = this.db.collection(GroundsService.collectionId);
    const farmsColletions = this.db.collection(FarmsService.collectionId);

    groundsColletion.doc(groundId).get()
      .subscribe(ground => {
        const groundAmount = ground.data().totalAmount + harvest.amount;

        farmsColletions.doc(ground.data().farmId).get()
          .subscribe(farm => {
            const farmAmount = farm.data().totalAmount + harvest.amount;
            ground.ref.update({ totalAmount: groundAmount })
            farm.ref.update({ totalAmount: farmAmount })
          })
        this.createHarvest(groundId, harvest);
      })
  }

  onRemoveHarvest(harvest: Harvest): void {
    const groundsColletion = this.db.collection(GroundsService.collectionId);
    const farmsColletions = this.db.collection(FarmsService.collectionId);

    groundsColletion.doc(harvest.groundId).get()
      .subscribe(ground => {
        const groundAmount = ground.data().totalAmount - harvest.amount;
        farmsColletions.doc(ground.data().farmId).get()
          .subscribe(farm => {
            const farmAmount = farm.data().totalAmount - harvest.amount;
            ground.ref.update({ totalAmount: groundAmount })
            farm.ref.update({ totalAmount: farmAmount })
          })        
        this.removeHarvest(harvest.id);
      })
  }  

}
