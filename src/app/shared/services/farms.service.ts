import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { Farm } from '../interfaces/farm';

@Injectable({
  providedIn: 'root'
})
export class FarmsService {

  static collectionId = 'farms';

  constructor(private db: AngularFirestore) {}

  createFarm(farm: Farm): Promise<DocumentReference> {
    return this.db.collection(FarmsService.collectionId)
      .add(farm);
  }

  updateFarm(farmId: string, farm: Farm): Promise<void> {
    return this.db.collection(FarmsService.collectionId)
      .doc(farmId)
      .update(farm);
  }  

  removeFarm(farmId: string): Promise<void> {
    return this.db.collection(FarmsService.collectionId)
      .doc(farmId)
      .delete();
  }    

  getFarms(): Observable<{ id: string; }[]> {
    return this.db.collection(FarmsService.collectionId)
      .valueChanges({ idField: 'id' });
  }

  getFarmById(farmId: string): AngularFirestoreDocument<Farm> {
    return this.db.collection(FarmsService.collectionId)
      .doc(farmId);    
  }

  getRef(): DocumentReference {
    return this.db.collection(FarmsService.collectionId).ref.doc();
  }
}
