import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { Ground } from '../interfaces/ground';
import { FarmsService } from '../services/farms.service';

@Injectable({
  providedIn: 'root'
})
export class GroundsService {

  static collectionId = 'grounds';

  constructor(private db: AngularFirestore) {}

  createGround(farmId: string, ground: Ground): Promise<DocumentReference> {
    return this.db.collection(GroundsService.collectionId)
      .add({ ...ground, farmId })
  }

  updateGround(groundId: string, ground: Ground): Promise<void> {
    return this.db.collection(GroundsService.collectionId)
      .doc(groundId)
      .update(ground);
  }  

  removeGround(groundId: string): Promise<void> {
    return this.db.collection(GroundsService.collectionId)
      .doc(groundId)
      .delete();
  }    

  getGrounds(): Observable<Ground> {
    return this.db.collection(GroundsService.collectionId)
      .valueChanges({ idField: 'id' });
  }

  getGroundById(groundId: string): AngularFirestoreDocument<Ground> {
    return this.db.collection(GroundsService.collectionId)
      .doc(groundId);    
  }

  getGroundsByFarmId(farmId: string): Observable<Ground[]> {
    return this.db.collection(GroundsService.collectionId, ref =>
      ref.where('farmId', '==', farmId)
    )
    .valueChanges({ idField: 'id' })
  }  

  onRemoveGround(ground: Ground): void {
    const farmsColletions = this.db.collection(FarmsService.collectionId);

    farmsColletions.doc(ground.farmId).get()
      .subscribe(farm => {
        const farmAmount = farm.data().totalAmount - ground.totalAmount;
        farm.ref.update({ totalAmount: farmAmount })
        this.removeGround(ground.id);
      }
    )
  }
}
